package com.nanda.locationmanager;


import android.support.annotation.NonNull;
import android.support.annotation.Size;
import android.text.TextUtils;
import android.util.Log;


/**
 * A log utility works with Build Type.
 * <p>
 * Created by Saurabh on 17/01/18.
 */
public class Logger {

    public static final String MESSAGE_IS_NULL = "Message is null.";

    public static void e(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Log.e(tag, message);
        }
    }

    public static void d(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Log.d(tag, message);
        }
    }

    public static void i(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Log.i(tag, message);
        }
    }


    public static void w(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Log.w(tag, message);
        }
    }


    public static void v(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Log.v(tag, message);
        }
    }

    /**
     * <b>Log Network Event</b>
     * Log network call request response event.
     * All log will be logged as error and with tag <b><i>network</i></b>.
     *
     * @param message
     */
    public static void logNetworkEvent(@NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Logger.e("Network", message);
        }
    }

    /**
     * Log event without caring about <b>Build Type</b>.
     *
     * @param tag
     * @param message
     */
    public static void logIndependentEvent(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
        Log.e(tag, message);
    }

}
