package com.nanda.locationmanager;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;


/**
 * Created by Saurabh on 30/09/18.
 */
public class Prefs {

    private static Prefs prefs;
    private SharedPreferences sharedPreferences;

    private Prefs() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.getAppContext());
    }

    public static Prefs instanceOf() {
        if (prefs == null) {
            prefs = new Prefs();
        }
        return prefs;
    }


    /**
     * Get data from prefs with key {key} & of type {obj}
     *
     * @param key
     * @param obj
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> T getDataWithKey(String key, T obj) {
        if (obj instanceof String) {
            return (T) sharedPreferences.getString(key, (String) obj);
        } else if (obj instanceof Integer) {
            return (T) (Integer) sharedPreferences.getInt(key, (Integer) obj);
        } else if (obj instanceof Boolean) {
            return (T) (Boolean) sharedPreferences.getBoolean(key, (Boolean) obj);
        }
        return null;
    }

    /**
     * Save data to prefs with key {key} & of type {obj}
     *
     * @param key
     * @param obj
     * @param <T>
     * @return
     */
    public <T> void putDataWithKey(String key, T obj) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (obj instanceof String) {
            editor.putString(key, (String) obj);
        } else if (obj instanceof Integer) {
            editor.putInt(key, (Integer) obj);
        } else if (obj instanceof Boolean) {
            editor.putBoolean(key, (Boolean) obj);
        } else if (obj instanceof Float) {
            editor.putFloat(key, (Float) obj);
        }
        editor.commit();
    }



    public void clearPref() {
        sharedPreferences.edit().clear().commit();
    }


    public void setLocationCoveredDistanceInMeter(String distance) {
        putDataWithKey(Keys.DEVICE_DISTANCE_COVER, distance);
    }


    public String getLocationCoveredDistanceInMeter() {
        return getDataWithKey(Keys.DEVICE_DISTANCE_COVER, "0");
    }



    /**
     * This Class will contains all keys going to use in preferences.
     */
    public static class Keys {
        public static final String DEVICE_DISTANCE_COVER = "device_dis_cover";
    }
}




