package com.nanda.locationmanager;

import android.database.Cursor;

public class Constants {
    public static final String ACTION_STARTFOREGROUND_ACTION = "action_start";
    public static final String ACTION_STOPFOREGROUND_ACTION = "action_stop";


    public static void closeCursor(Cursor cursor) {

        if (cursor != null && !cursor.isClosed())
            cursor.close();

    }
}
