package com.nanda.locationmanager.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.nanda.locationmanager.Constants;
import com.nanda.locationmanager.R;
import com.nanda.locationmanager.activity.HomeActivity;
import com.nanda.locationmanager.helper.LocationHelper;
import com.nanda.locationmanager.helper.LocationManager;
import com.nanda.locationmanager.model.LatLangModel;
import com.nanda.locationmanager.provider.DBUtility;
import com.nanda.locationmanager.receiver.LocationBroadcastHelper;

public class LocationUpdateService extends Service implements LocationManager {
    public static final String CHANNEL_ID = "LocationUpdateService";
    private LocationHelper locationHelper;
    private NotificationManager mNotificationManager;
    private String LOG_TAG = "LocationUpdateService";

    @Override
    public void onCreate() {
        super.onCreate();
        locationHelper = new LocationHelper(LocationUpdateService.this, this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

            if (intent.getAction().equals(Constants.ACTION_STARTFOREGROUND_ACTION)) {
                Log.i(LOG_TAG, "Received Start Foreground Intent ");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                startForeground(101, prepareNotification());

                locationHelper.startLocationUpdates();
            } else if (intent.getAction().equals(Constants.ACTION_STOPFOREGROUND_ACTION)) {
                Log.i(LOG_TAG, "Received Stop Foreground Intent");
                locationHelper.stopLocationUpdates();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                stopForeground(true);

                stopSelf();

        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void getLastKnownLocation(Location lastLocation) {
        Log.d("lastK nown", "lastK nown- " + lastLocation.getLongitude());
        sendBroadCast(LocationBroadcastHelper.KEY_LAST_KNOWN_LOCATION_UPDATE_EVENT, lastLocation);

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "onLocationChanged- " + location.getLongitude());
        LatLangModel latLangModel = new LatLangModel();
        latLangModel.setRideId("1");
        latLangModel.setLatitude(location.getLatitude());
        latLangModel.setLongitude(location.getLongitude());
        DBUtility.insertLatLong(latLangModel);
        sendBroadCast(LocationBroadcastHelper.KEY_CURRENT_LOCATION_UPDATE_EVENT, location);
    }


    private void sendBroadCast(String action, Location location){
        LatLangModel latLangModel = new LatLangModel();
        latLangModel.setLatitude(location.getLatitude());
        latLangModel.setLongitude(location.getLongitude());
        Intent intent = new Intent();
        intent.setAction(action);
        intent.putExtra(LocationBroadcastHelper.EXTRA_DATA, latLangModel);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationHelper.stopLocationUpdates();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            stopForeground(true);
        }
    }

    private Notification prepareNotification() {
        // handle build version above android oreo
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O &&
                mNotificationManager.getNotificationChannel(CHANNEL_ID) == null) {
            CharSequence name = "ABC";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.enableVibration(false);
            mNotificationManager.createNotificationChannel(channel);
        }


        // notification builder
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        } else {
            notificationBuilder = new NotificationCompat.Builder(this);
        }*/
        notificationBuilder
                .setSmallIcon(R.mipmap.ic_launcher)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setOnlyAlertOnce(true)
                .setOngoing(true)
                .setAutoCancel(true);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        }

        return notificationBuilder.build();
    }
}

