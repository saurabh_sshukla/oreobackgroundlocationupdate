package com.nanda.locationmanager.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

public class LocationBroadcastHelper {

    public static final String KEY_LAST_KNOWN_LOCATION_UPDATE_EVENT = "com.nanda.locationmanager.receiver.LAST_KNOWN_LOCATION_UPDATE_EVENT";
    public static final String KEY_CURRENT_LOCATION_UPDATE_EVENT = "com.nanda.locationmanager.receiver.CURRENT_LOCATION_UPDATE_EVENT";
    public static final String EXTRA_DATA = "extra_data";
    public static final int TYPE_LAST_KNOWN_LOCATION = 0;
    public static final int TYPE_CURRENT_LOCATION = 1;

    private static LocationBroadcastHelper mLocationBroadcastHelper;

    private LocationBroadcastHelper() {
    }


    public static LocationBroadcastHelper getInstance() {
        if (mLocationBroadcastHelper == null) { //if there is no instance available... create new one
            mLocationBroadcastHelper = new LocationBroadcastHelper();
        }
        return mLocationBroadcastHelper;
    }

    public void setLocationUpdateReceiver(BroadcastReceiver receiver, Activity activity) {
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(KEY_CURRENT_LOCATION_UPDATE_EVENT);
        mIntentFilter.addAction(KEY_LAST_KNOWN_LOCATION_UPDATE_EVENT);
        if (receiver != null && activity != null) {
            LocalBroadcastManager.getInstance(activity).registerReceiver(receiver, mIntentFilter);
        }
    }

    public void unRegisterReceiver(Activity activity, BroadcastReceiver receiver) {
        if (receiver != null && activity != null) {
            LocalBroadcastManager.getInstance(activity).unregisterReceiver(receiver);
        }
    }



}
