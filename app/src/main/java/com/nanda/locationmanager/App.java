package com.nanda.locationmanager;


import android.app.Application;
import android.content.Context;


public class App extends Application {

    private static final String TAG = App.class.getSimpleName();
    private static Context mApplicationContext;


    private static App mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationContext = this;


        mInstance = this;
    }

    public static synchronized App getInstance() {
        return mInstance;
    }



    /**
     * Get Application Context.
     *
     * @return
     */
    public static Context getAppContext() {
        return mApplicationContext;
    }



}



