package com.nanda.locationmanager.model;

import android.os.Parcel;
import android.os.Parcelable;

public class LatLangModel implements Parcelable {

    private  double latitude;

    private  double longitude;

    private String rideId;

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }



    public LatLangModel(){}


    protected LatLangModel(Parcel in) {
        latitude = in.readDouble();
        longitude = in.readDouble();
        rideId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(rideId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<LatLangModel> CREATOR = new Parcelable.Creator<LatLangModel>() {
        @Override
        public LatLangModel createFromParcel(Parcel in) {
            return new LatLangModel(in);
        }

        @Override
        public LatLangModel[] newArray(int size) {
            return new LatLangModel[size];
        }
    };
}