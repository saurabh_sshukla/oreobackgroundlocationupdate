package com.nanda.locationmanager.provider;

import android.database.Cursor;

import com.nanda.locationmanager.App;
import com.nanda.locationmanager.provider.contract.CabContract;

/**
 * Created by Saurabh on 26/09/18.
 */

public class DataBaseManager {

    public static final String EQUALS_WHAT = " = ? ";
    public static final String COMMA = " , ";
    public static final String AND = " AND ";
    public static final String OR = " OR ";


    /**
     * Check whether property of given id exist or not.
     *
     * @param id Id of metadata to be checked.
     * @return whether property with given id exist or not.
     */
    public static boolean isRideIdExist(String id) throws Exception {

        String sel = CabContract.Locations.COL_RIDE_ID + EQUALS_WHAT;

        String[] args = new String[]{String.valueOf(id)};

        String[] proj = new String[]{CabContract.Locations.COL_RIDE_ID};

        Cursor cursor = App.getAppContext().getContentResolver().query(CabContract.Locations.CONTENT_URI, proj, sel, args, null);

        return cursor != null && cursor.getCount() > 0;

    }
}
