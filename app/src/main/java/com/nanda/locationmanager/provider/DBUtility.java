package com.nanda.locationmanager.provider;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.nanda.locationmanager.App;
import com.nanda.locationmanager.Constants;
import com.nanda.locationmanager.model.LatLangModel;
import com.nanda.locationmanager.provider.contract.CabContract;

import java.util.ArrayList;
import java.util.List;

import static com.nanda.locationmanager.provider.DataBaseManager.EQUALS_WHAT;

public class DBUtility {

    /**
     * Insert Update User.
     *
     * @param latLangModel
     */
    public static void insertUpdateLatLong(LatLangModel latLangModel) {

        String selection = CabContract.Locations.COL_RIDE_ID + EQUALS_WHAT;

        try {

            if (latLangModel == null) return;

            if (DataBaseManager.isRideIdExist(latLangModel.getRideId())) {
                App.getAppContext().getContentResolver().update(CabContract.Locations.CONTENT_URI, getUserContentValues(latLangModel), selection, new String[]{String.valueOf(latLangModel.getRideId())});

            } else {
                App.getAppContext().getContentResolver().insert(CabContract.Locations.CONTENT_URI, getUserContentValues(latLangModel));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Insert Update User.
     *
     * @param latLangModel
     */
    public static void insertLatLong(LatLangModel latLangModel) {
        try {

            if (latLangModel == null) return;

            App.getAppContext().getContentResolver().insert(CabContract.Locations.CONTENT_URI, getUserContentValues(latLangModel));
            Log.e("Insert ", "Insert-  "+ latLangModel.getLatitude());

        } catch (Exception e) {
            Log.e("ERROR ", "DB ERROR-  "+ latLangModel.getLatitude());
            e.printStackTrace();
        }

    }



    /**
     * Get Content Values for User.
     *
     * @param user
     * @return
     */
    private static ContentValues getUserContentValues(LatLangModel user) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CabContract.Locations.COL_RIDE_ID, user.getRideId());
        contentValues.put(CabContract.Locations.COL_LATITUDE, user.getLatitude());
        contentValues.put(CabContract.Locations.COL_LONGITUDE, user.getLongitude());
        return contentValues;
    }


    /**
     * Get Logged in user details
     *
     * @param rideId is user id of currently logged in user
     * @return object of User
     */
    public static LatLangModel getLongByRideId(String rideId) {
        LatLangModel user = new LatLangModel();

        Cursor cursor = App.getAppContext().getContentResolver().query(
                CabContract.Locations.CONTENT_URI,
                null,
                CabContract.Locations.COL_RIDE_ID + EQUALS_WHAT,
                new String[]{rideId}, null);

        try {
            if (cursor != null && !cursor.isClosed() && cursor.moveToFirst()) {
                user.setRideId(cursor.getString(cursor.getColumnIndex(CabContract.Locations.COL_RIDE_ID)));
                user.setLatitude(Double.parseDouble(cursor.getString(cursor.getColumnIndex(CabContract.Locations.COL_LATITUDE))));
                user.setLongitude(Double.parseDouble(cursor.getString(cursor.getColumnIndex(CabContract.Locations.COL_LATITUDE))));

            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {
            Constants.closeCursor(cursor);
        }

        return user;
    }

    public static ArrayList<LatLangModel> getAllLatLongByRideId() {
        ArrayList<LatLangModel> latLangModelList = new ArrayList<>();


        Cursor cursor = App.getAppContext().getContentResolver().query(
                CabContract.Locations.CONTENT_URI,
                null,
                null,
               null, null);


        try {

                if (cursor != null && !cursor.isClosed() && cursor.moveToFirst()) {
                    if (cursor.moveToFirst()) {
                        do {
                            LatLangModel user = new LatLangModel();
                            user.setRideId(cursor.getString(cursor.getColumnIndex(CabContract.Locations.COL_RIDE_ID)));
                            user.setLatitude(Double.parseDouble(cursor.getString(cursor.getColumnIndex(CabContract.Locations.COL_LATITUDE))));
                            user.setLongitude(Double.parseDouble(cursor.getString(cursor.getColumnIndex(CabContract.Locations.COL_LONGITUDE))));
                            latLangModelList.add(user);
                        } while (cursor.moveToNext());
                    }
                }

            } catch (Exception e) {

                e.printStackTrace();

            } finally {
                Constants.closeCursor(cursor);
            }


        return latLangModelList;
    }

    public static int deleteAllRecord(){
        int rows = 0;
        try {
            rows =  App.getAppContext().getContentResolver().delete(CabContract.Locations.CONTENT_URI, null, null);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return rows;
    }


}
