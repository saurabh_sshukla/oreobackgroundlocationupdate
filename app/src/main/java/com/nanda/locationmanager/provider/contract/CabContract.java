package com.nanda.locationmanager.provider.contract;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

import com.nanda.locationmanager.BuildConfig;
import com.nanda.locationmanager.provider.Route;

public class CabContract implements Route {

    /**
     * Content provider authority.
     */
    public static final String AUTHORITY = BuildConfig.APPLICATION_ID + ".providers";

    /**
     * Base URI. (content://com.example.android.basicsyncadapter)
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);


    /**
     * This Class will have information about user table.
     */
    public static class Locations implements BaseColumns {

        /**
         * MIME type for lists of Users.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd." + AUTHORITY + ".latlang";

        /**
         * MIME type for individual user.
         */
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd." + AUTHORITY + ".latlang";

        /**
         * Fully qualified URI for "user" resources.
         */
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER).build();

        /**
         * Table name where records are stored for "user" resources.
         */
        public static final String TABLE_NAME = "LocationLalLang";

        /**
         * Column UserId.
         */
        public static final String COL_RIDE_ID = "ride_id";

        /**
         * Column First Name.
         */
        public static final String COL_LATITUDE = "latitude";

        /**
         * Column Last Name.
         */
        public static final String COL_LONGITUDE = "longitude";


    }
}
