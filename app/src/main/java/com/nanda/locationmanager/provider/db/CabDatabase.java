package com.nanda.locationmanager.provider.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nanda.locationmanager.Logger;

/**
 * Created by Saurabh on 26/09/18.
 */

public class CabDatabase extends SQLiteOpenHelper implements CabDBQueryBuilder {
    public static final String TAG = CabDatabase.class.getSimpleName();

    /**
     * Schema version.
     */
    public static final int DATABASE_VERSION = 1;

    /**
     * Filename for SQLite file.
     */
    public static final String DATABASE_NAME = "location.db";


    public CabDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Logger.d(TAG, "Database Create Start.");

        sqLiteDatabase.execSQL(CREATE_USER);
        Logger.d(TAG, CREATE_USER);

        Logger.d(TAG, "Database Create End.");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Logger.d(TAG, "Database Upgrade Start.");

        sqLiteDatabase.execSQL(DELETE_USER);
        Logger.d(TAG, DELETE_USER);

        Logger.d(TAG, "Database Upgrade End.");
    }
}
