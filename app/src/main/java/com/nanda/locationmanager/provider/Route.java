package com.nanda.locationmanager.provider;

/**
 * Created by Saurabh on 26/09/18.
 */

public interface Route {

    // The constants below represent individual URI routes, as IDs. Every URI pattern recognized by
    // this ContentProvider is defined using sUriMatcher.addURI(), and associated with one of these
    // IDs.
    //
    // When a incoming URI is run through sUriMatcher, it will be tested against the defined
    // URI patterns, and the corresponding route ID will be returned.

    /**
     * URI ID for User: /users
     */
    int ROUTE_USER = 1;
    String PATH_USER = "latlang";

    /**
     * URI ID for route: /user/{ID}
     */
    int ROUTE_USER_ID = 2;
    String PATH_USER_ID = "latlang/*";




}
