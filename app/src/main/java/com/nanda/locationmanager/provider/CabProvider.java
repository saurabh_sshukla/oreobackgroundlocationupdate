package com.nanda.locationmanager.provider;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.nanda.locationmanager.App;
import com.nanda.locationmanager.provider.contract.CabContract;
import com.nanda.locationmanager.provider.db.CabDatabase;
import com.nanda.locationmanager.provider.db.SelectionBuilder;

import java.util.ArrayList;

import static com.nanda.locationmanager.provider.contract.CabContract.AUTHORITY;


public class CabProvider extends ContentProvider implements Route {

    private CabDatabase dbHelper;

    /**
     * UriMatcher, used to decode incoming URIs.
     */
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(AUTHORITY, PATH_USER, ROUTE_USER);
        uriMatcher.addURI(AUTHORITY, PATH_USER_ID, ROUTE_USER_ID);
    }

    @Override
    public boolean onCreate() {
        dbHelper = new CabDatabase(getContext());
        return false;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {

        final int match = uriMatcher.match(uri);

        switch (match) {

            case ROUTE_USER:
                return CabContract.Locations.CONTENT_TYPE;
            case ROUTE_USER_ID:
                return CabContract.Locations.CONTENT_ITEM_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        SelectionBuilder builder = buildSimpleSelection(uri);

        final int match = uriMatcher.match(uri);

        switch (match) {

            case ROUTE_USER:
                return getCursor(uri, projection, selection, selectionArgs, sortOrder, db);

            case ROUTE_USER_ID:
                String uId = uri.getLastPathSegment();
                builder.where(CabContract.Locations.COL_RIDE_ID + "=?", uId);
                return builder.query(db, projection, sortOrder);


            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        final int match = uriMatcher.match(uri);
        Uri result;

        switch (match) {

            case ROUTE_USER:
                result = Uri.parse(uri + "/" + db.insertOrThrow(getTableByRouteId(match), null, contentValues));
                break;

            case ROUTE_USER_ID:
            default:
                throw new UnsupportedOperationException("Unsupported Operation : " + uri);
        }

        App.getAppContext().getContentResolver().notifyChange(uri, null, false);

        return result;

    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SelectionBuilder builder = buildSimpleSelection(uri);

        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        final int match = uriMatcher.match(uri);

        int count = 0;

        switch (match) {

            case ROUTE_USER:
                count = builder.table(getTableByRouteId(match))
                        .where(selection, selectionArgs)
                        .delete(db);
                break;

            case ROUTE_USER_ID:
                builder.table(getTableByRouteId(match))
                        .where(CabContract.Locations.COL_RIDE_ID + "=?", uri.getLastPathSegment())
                        .where(selection, selectionArgs)
                        .delete(db);
                break;

            default:
                throw new UnsupportedOperationException("Unsupported Operation : " + uri);
        }

        App.getAppContext().getContentResolver().notifyChange(uri, null, false);


        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {

        SelectionBuilder builder = buildSimpleSelection(uri);

        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        final int match = uriMatcher.match(uri);

        int count = 0;

        switch (match) {

            case ROUTE_USER:
                count = builder.table(getTableByRouteId(match))
                        .where(selection, selectionArgs)
                        .update(db, contentValues);
                break;

            case ROUTE_USER_ID:
                builder.table(getTableByRouteId(match))
                        .where(CabContract.Locations.COL_RIDE_ID + "=?", uri.getLastPathSegment())
                        .where(selection, selectionArgs)
                        .update(db, contentValues);
                break;

            default:
                throw new UnsupportedOperationException("Unsupported Operation : " + uri);
        }

        App.getAppContext().getContentResolver().notifyChange(uri, null, false);

        return count;
    }


    @NonNull
    private Cursor getCursor(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, SQLiteDatabase db) {

        // Most cases are handled with simple SelectionBuilder
        final SelectionBuilder builder_ = buildSimpleSelection(uri);

        Cursor cursor = builder_.where(selection, selectionArgs).query(db, projection, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    /**
     * Build a simple {@link SelectionBuilder} to match the requested
     * {@link Uri}. This is usually enough to support {@link #insert},
     * {@link #update}, and {@link #delete} operations.
     */
    private SelectionBuilder buildSimpleSelection(Uri uri) {

        final SelectionBuilder builder = new SelectionBuilder();

        final int match = uriMatcher.match(uri);

        switch (match) {
            case ROUTE_USER:
            case ROUTE_USER_ID:
                return builder.table(CabContract.Locations.TABLE_NAME);
            default: {

                throw new UnsupportedOperationException("Unknown uri: " + uri);

            }
        }
    }


    /**
     * Get article table by route id.
     *
     * @param match
     * @return
     */

    private String getTableByRouteId(int match) {
        switch (match) {
            case ROUTE_USER_ID:
            case ROUTE_USER:
                return CabContract.Locations.TABLE_NAME;
            default:
                return "";
        }
    }

    /**
     * Apply the given set of {@link ContentProviderOperation}, executing inside
     * a {@link SQLiteDatabase} transaction. All changes will be rolled back if
     * any single one fails.
     */
    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                results[i] = operations.get(i).apply(this, results, i);
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }
}

