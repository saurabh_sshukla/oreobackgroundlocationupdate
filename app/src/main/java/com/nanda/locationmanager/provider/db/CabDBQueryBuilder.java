package com.nanda.locationmanager.provider.db;


import com.nanda.locationmanager.provider.contract.CabContract;

/**
 * Created by Saurabh on 26/09/18.
 */
public interface CabDBQueryBuilder {

    String TYPE_TEXT = " TEXT";
    String TYPE_INTEGER = " INTEGER";
    String TYPE_LONG = " LONG";
    String TYPE_REAL = " REAL";
    String TYPE_LONG_DETAULT_0 = " LONG DEFAULT '0'";
    String TYPE_INTEGER_DEFAULT_0 = " INTEGER DEFAULT '0' ";
    String TYPE_INTEGER_DEFAULT_1 = " INTEGER DEFAULT '1' ";
    String TYPE_INTEGER_PRIMARY_KEY = " INTEGER PRIMARY KEY";
    String TYPE_TEXT_PRIMARY_KEY = " TEXT PRIMARY KEY";
    String TYPE_LONG_PRIMARY_KEY = " LONG PRIMARY KEY";
    String BRACE_OPEN = " (";
    String BRACE_CLOSE = ")";
    String COMMA_SEP = ",";


    /**
     * SQL statement to create user table.
     */
    String CREATE_USER = "CREATE TABLE " + CabContract.Locations.TABLE_NAME + BRACE_OPEN
            + CabContract.Locations.COL_RIDE_ID + TYPE_TEXT + COMMA_SEP +
            CabContract.Locations.COL_LATITUDE + TYPE_TEXT + COMMA_SEP +
            CabContract.Locations.COL_LONGITUDE + TYPE_TEXT + BRACE_CLOSE;


    /**
     * SQL statement to drop "User" table.
     */
    String DELETE_USER = "DROP TABLE IF EXISTS " + CabContract.Locations.TABLE_NAME;


}
