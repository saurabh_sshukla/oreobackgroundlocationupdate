package com.nanda.locationmanager.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.util.DbUtils;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.nanda.locationmanager.Constants;
import com.nanda.locationmanager.Prefs;
import com.nanda.locationmanager.R;
import com.nanda.locationmanager.base.BaseActivity;
import com.nanda.locationmanager.model.LatLangModel;
import com.nanda.locationmanager.provider.DBUtility;
import com.nanda.locationmanager.receiver.LocationBroadcastHelper;
import com.nanda.locationmanager.service.LocationUpdateService;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity {
    private Location previousLocation;
    private Location currentLocation;
    private ArrayList<LatLangModel> locationArrayList = new ArrayList<LatLangModel>();
    private TextView txtLocation, DBTV;
    private static final float MINIMUM_ACCURACY = 10; // If the accuracy of the measurement is worse than 10 meters, we will not take the location into account
    private static final float MINIMUM_DISTANCE_BETWEEN_POINTS = 5; // If the user hasn't moved at least 10 meters, we will not take the location into account


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        this.txtLocation = findViewById(R.id.txtRunningLocationMetrix);
        this.DBTV = findViewById(R.id.db_saved_dis);


        DBTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calculateDistance();
            }
        });
    }


    private void callPermission() {

        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        String rationale = "Location permission are required to get User Location";
        Permissions.Options options = new Permissions.Options()
                .setRationaleDialogTitle("Location Permission")
                .setSettingsDialogTitle("Warning");


        Permissions.check(this, permissions, rationale, options, new PermissionHandler() {
            @Override
            public void onGranted() {
                // do your task.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    Intent startIntent = new Intent(HomeActivity.this, LocationUpdateService.class);
                    startIntent.setAction(Constants.ACTION_STARTFOREGROUND_ACTION);
                    startForegroundService(startIntent);
                } else {
                    Intent intent = new Intent(HomeActivity.this, LocationUpdateService.class);
                    intent.setAction(Constants.ACTION_STARTFOREGROUND_ACTION);
                    startService(intent);
                }
            }

            @Override
            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                // permission denied, block the feature.
                callPermission();
            }
        });
    }


    @OnClick(R.id.btn_start_track)
    public void onTrackingButtonClicked() {
        callPermission();
    }

    @OnClick(R.id.btn_stop_track)
    public void onStopTrackingButtonClicked() {
        stopLocationupdate();
    }

    private void stopLocationupdate() {
        Intent intent = new Intent(HomeActivity.this, LocationUpdateService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent stopIntent = new Intent(HomeActivity.this, LocationUpdateService.class);
            stopIntent.setAction(Constants.ACTION_STOPFOREGROUND_ACTION);
            startService(stopIntent);
        } else {
            Intent stopIntent = new Intent(HomeActivity.this, LocationUpdateService.class);
            stopIntent.setAction(Constants.ACTION_STOPFOREGROUND_ACTION);
            stopService(intent);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        LocationBroadcastHelper.getInstance().setLocationUpdateReceiver(mBroadcastReceiver, this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocationBroadcastHelper.getInstance().unRegisterReceiver(this, mBroadcastReceiver);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.hasExtra(LocationBroadcastHelper.EXTRA_DATA)) {
                if (intent.getAction().equalsIgnoreCase(LocationBroadcastHelper.KEY_LAST_KNOWN_LOCATION_UPDATE_EVENT)) {
                    LatLangModel latLangModel = intent.getParcelableExtra(LocationBroadcastHelper.EXTRA_DATA);
                    Log.e("receive", "last- " + latLangModel.getLatitude() + "/" + latLangModel.getLongitude());

                } else if (intent.getAction().equals(LocationBroadcastHelper.KEY_CURRENT_LOCATION_UPDATE_EVENT)) {
                    LatLangModel latLangModel = intent.getParcelableExtra(LocationBroadcastHelper.EXTRA_DATA);
                    Log.e("receive current", "- " + latLangModel.getLatitude() + "/" + latLangModel.getLongitude());
                    previousLocation = currentLocation;
                    Location location = new Location("");
                    location.setLatitude(latLangModel.getLatitude());
                    location.setLongitude(latLangModel.getLongitude());
                    currentLocation = location;

                    if (previousLocation != null) {
                        Location locationA = new Location("point A");

                        locationA.setLatitude(previousLocation.getLatitude());
                        locationA.setLongitude(previousLocation.getLongitude());

                        Location locationB = new Location("point B");

                        locationB.setLatitude(currentLocation.getLatitude());
                        locationB.setLongitude(currentLocation.getLongitude());

                        previousLocation = currentLocation;

                        float distance = locationA.distanceTo(locationB);

                        double totalCover = Double.parseDouble(Prefs.instanceOf().getLocationCoveredDistanceInMeter()) + distance;
                        Prefs.instanceOf().setLocationCoveredDistanceInMeter(String.valueOf(totalCover));
                        txtLocation.setText("" + getDistanceInKm(totalCover));

                    }

                }

            }
        }
    };

    public String getDistanceInKm(double totalDistance) {
        if (totalDistance == 0.0 || totalDistance < -1)
            return "0 KM";
        else if (totalDistance > 0 && totalDistance < 1000)
            return String.valueOf(totalDistance) + " Meters";
        DecimalFormat df = new DecimalFormat("#.###");
        return df.format(totalDistance / 1000) + " KM";
    }




    private  double  totalDis = 0;
    private void calculateDistance(){
        locationArrayList =  DBUtility.getAllLatLongByRideId();
        if(locationArrayList.size() > 1){
            for(int i=0; i<locationArrayList.size()-1; i++){
                LatLangModel latLangModelOrigin = locationArrayList.get(i);
                LatLangModel latLangModelDestination = locationArrayList.get(i+1);


                        Location locationA = new Location("point A");

                        locationA.setLatitude(latLangModelOrigin.getLatitude());
                        locationA.setLongitude(latLangModelOrigin.getLongitude());

                        Location locationB = new Location("point B");

                        locationB.setLatitude(latLangModelDestination.getLatitude());
                        locationB.setLongitude(latLangModelDestination.getLongitude());
                // if (currentLocation.hasAccuracy() && currentLocation.getAccuracy() < MINIMUM_ACCURACY) {
                if ( locationA.distanceTo(locationB) > MINIMUM_DISTANCE_BETWEEN_POINTS) {
                        float distance = locationA.distanceTo(locationB);
                        totalDis = totalDis + distance;
                        DBTV.setText("" + getDistanceInKm(totalDis));
                    }
             //   }

            }
            stopLocationupdate();
            locationArrayList.clear();
            Log.e("Delete All ",""+DBUtility.deleteAllRecord());
        }

    }

}
