package com.nanda.locationmanager.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

public class LocationHelper {

    private static final int INTERVAL = 10 * 1000;
    private static final int FAST_INTERVAL = 5 * 1000;


    private Context activity;
    private FusedLocationProviderClient fusedLocationClient;

    private LocationManager locationManager;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;

    public LocationHelper(Context activity, LocationManager locationManager) {
        this.activity = activity;
        this.locationManager = locationManager;
        if (fusedLocationClient == null) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        }
        createLocationRequest();
        createLocationCallBack();
    }

    private void createLocationCallBack() {
        if (locationCallback == null) {
            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    if (locationResult != null) {
                        for (Location location : locationResult.getLocations()) {
                            if (location != null && locationManager != null) {
                                if (locationManager != null) {
                                    locationManager.onLocationChanged(location);
                                }
                            }
                        }
                    }
                }
            };
        }
    }

    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(INTERVAL);
        locationRequest.setFastestInterval(FAST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void startLocationUpdates() {
        try {
            fusedLocationClient.requestLocationUpdates(locationRequest,
                    locationCallback,
                    null /* Looper */);
            getLastKnownLocation();
        }catch (SecurityException e){
            e.printStackTrace();
        }
    }

    public void stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }

   private void getLastKnownLocation() {
        if (fusedLocationClient == null) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        }

         try {
             fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                 @Override
                 public void onSuccess(Location location) {
                     if (location != null) {
                         if (locationManager != null) {
                             locationManager.getLastKnownLocation(location);
                         }
                     }
                 }
             });
         }catch (SecurityException e){}
    }
}
